/*
 * @Author: qinuoyun
 * @Date:   2019-09-28 13:29:03
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-19 23:41:58
 */

class common {
    /**
     * 构造函数
     * @return {[type]} [description]
     */
    constructor() {
        const _config = require('./config.js');
        const file = ROOT_DIR + '/src/config.js';
        if (is_file(file)) {
            this.config = extend(_config, require(file));
        } else {
            this.config = _config;
        }
    }


    /**
     * 设置
     * @param {[type]} model [description]
     * @param {[type]} name  [description]
     * @param {[type]} value [description]
     */
    set(name = null, value = null) {
        //判断如果传入的是数组，则叠加覆盖
        if (is_array(name)) {
            this.config = extend(this.config, name);
            return;
        }
        //判断：如果传入的新配置项是字符串，则将字符串转换为数组
        if (name && is_string(name)) {
            //判断：如果传入的字符串没有点
            if (!strstr(name, ".")) {
                //判断：传入的配置项的值为空，则查找该配置是否存在，如果存在将该配置项设置为空
                if (is_null(value)) {
                    return isset(this.config[name]) ? this.config[name] : null;
                } else {
                    this.config[name] = value;
                    return;
                }
            } else {
                //处理数据值
                let keyword_array = explode(".", name);
                if (value) {
                    let end_key = end(keyword_array);
                    let data = this.config;
                    let _key = "";
                    //循环处理key
                    for (let index in keyword_array) {
                        let key = keyword_array[index];
                        if (key == end_key) {
                            if (_key) {
                                data[_key][key] = value;
                            } else {
                                data[key] = value;
                            }
                        } else {
                            //处理数据对象
                            if (_key) {
                                data = data[_key]
                                _key = key;
                            } else {
                                _key = key;
                            }
                        }
                    }
                    return this.config;
                } else {
                    //标识获取参数值
                    let data = this.config;
                    for (let index in keyword_array) {
                        let key = keyword_array[index];
                        data = data[key];
                    }
                    return data;
                }
            }
        }
        //判断配置器是否为空
        if (!empty(this.config)) {
            //判断：如果没有传入新的配置项，则返回该配置项
            if (is_null(name)) {
                return this.config;
            } else {
                return this.config[name];
            }
        }
        return false;
    }
}

const _concll = new common();

global.C = function(model = null, name = null, value = null) {
    return _concll.set(model, name, value)
}