/*
 * @Author: qinuoyun
 * @Date:   2021-09-16 09:38:42
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-10-22 15:29:55
 */
module.exports = {
    "port": "9000",
    //可以设置mysql,mongodb,false
    "database": {
        "dialect": "",
        "hostname": "", //支持数组
        "database": "",
        "username": "", //可以为空
        "password": "",
        "replicaSet": "",
        "hostport": "",
        "tableprefix": ""
    },
    "components": {
        //JWT用户认证
        "jwt": {
            "class": "",
            "key": "qinuoyun",
            "leeway": 20
        },
        //用户模块
        "user": {
            "identityClass": "controllers/LoginController",
            "enableAutoLogin": true,
            "enableSession": false,
        },
        //即时通讯模块
        "socket": {
            "class": "controllers/SocketController",
            "enableConnection": true
        }
    },
    /**
     * 处理页面白名单信息
     */
    "whitelist": [],
    "cache": {
        "path": "temp/file",
        "type": "file"
    }
}