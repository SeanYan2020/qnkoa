/*
 * @Author: seanyan
 * @Date:   2021-09-13 16:21:18
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-10-12 17:18:05
 */
const router = require('koa-router')()

/**
 * 渲染页面接口
 * @param {[type]} next)
 * @yield {[type]} [description]
 */
router.get('/', async (ctx, next) => {
    ctx.body = '欢迎使用QINUOYUN框架'
})

/**
 * 渲染页面接口
 * @param {[type]} next)
 * @yield {[type]} [description]
 */
router.get('/admin', async (ctx, next) => {
    let title = '后台界面';
    await ctx.render('admin', {
        title,
    })
})

/**
 * 渲染页面接口
 * @param {[type]} next)
 * @yield {[type]} [description]
 */
router.get('/designer', async (ctx, next) => {
    let title = '设计器界面';
    await ctx.render('designer', {
        title,
    })
})

module.exports = router;