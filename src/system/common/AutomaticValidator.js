/*
 * @Author: qinuoyun
 * @Date:   2021-09-23 22:50:03
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-23 23:02:02
 */
const {Validator} = require('./ValidatorModel.js')


class PersonValidator extends Validator {
    constructor(descriptor, models) {
        super()
        this.models = models;
        this.registration();
        //添加校验规则，参考async-validator
        this.descriptor = descriptor;
    }

    /**
     * 批量注册自定义方法
     */
    registration() {
        const Models = this.models
        Validator.register("unique", (rule, value, callback, source, options) => {
            new Promise((resolve, reject) => {
                let where = {};
                let key = rule.field;
                where[key] = value;
                Models.where(where).first().then(res => {
                    if (res) {
                        reject()
                    } else {
                        resolve()
                    }
                }).catch(error => {
                    reject(error)
                })
            }).then(res => {
                callback()
            }).catch(error => {
                callback(new Error())
            })
        })


    }
}

module.exports = PersonValidator