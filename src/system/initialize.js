/*
 * @Author: qinuoyun
 * @Date:   2019-09-26 13:37:10
 * @Last Modified by:   sean
 * @Last Modified time: 2021-09-15 23:46:21
 */
var path = require('path');

var os = require('os');

/**
 * 判断服务类型
 * @param  {[type]} obj [description]
 * @return {[type]}     [description]
 */
const is_server = function() {
    return typeof window !== 'undefined';
}

/**
 * 设置全局函数
 * @param  {Boolean} is_server() [description]
 * @return {[type]}              [description]
 */
if (is_server()) {
    window.define = function(key, value) {
        global[key] = value;
    }
    window.defined = function(value) {
        return typeof global[value] != "undefined" ? true : false;
    }
} else {
    global.define = function(key, value) {
        global[key] = value;
    }
    global.defined = function(value) {
        return typeof global[value] != "undefined" ? true : false;
    }
}

defined('DS') || define('DS', os.platform() == 'win32' ? "\\" : "/");

defined('DEBUG') || define('DEBUG', true);

defined('FRAME_DIR') || define('FRAME_DIR', path.resolve(__dirname));

defined('ROOT_DIR') || define('ROOT_DIR', path.resolve(__dirname, "../../"));

defined('VERSION') || define('VERSION', "V1.0.0");

defined('isProd') || define('isProd', process.env.NODE_ENV === 'production');