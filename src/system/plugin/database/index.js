/*
 * @Author: qinuoyun
 * @Date:   2021-10-14 16:37:58
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-12-27 22:18:33
 */

const fs = require("fs")
const path = require("path")
const { Sequelize, DataTypes, Op } = require('sequelize');

class database {
    /**
     * 实例对象
     * @type {String}
     */
    sequelize = null

    /**
     * 配置文件
     * @type {Object}
     */
    config = {}

    /**
     * 表格字段
     * @type {Object}
     */
    _tableFields = {}

    /**
     * 扩展说明
     * @type {{"<=": string, ne: string, lt: string, gte: string, eq: string, "<": string, lte: string, "!=": string, "=": string, gt: string, ">": string, ">=": string}}
     * @private
     */
    _exp = {
        "=": Op.eq,
        "eq": Op.eq,
        ">": Op.gt,
        "gt": Op.gt,
        "<": Op.lt,
        "lt": Op.lt,
        ">=": Op.gte,
        "gte": Op.gte,
        "<=": Op.lte,
        "lte": Op.lte,
        "!=": Op.ne,
        "in": Op.in,
        "ne": Op.ne,
        "is": Op.is,
        "not": Op.not,
        "notIn": Op.notIn,
        "or": Op.or,
        "like": Op.like,
        "notLike": Op.notLike,
    }

    /**
     * 构造函数
     * @param {[Object]} config对象
     */
    constructor(config) {
        //处理URI链接
        if (config && !this.sequelize && config.hostname) {
            this.config = config;
            this.sequelize = new Sequelize({
                "host": config.hostname,
                "port": config.hostport,
                "username": config.username,
                "password": config.password,
                "database": config.database,
                "dialect": config.dialect,
                "dialectOptions": {
                    "charset": 'utf8mb4',
                    "dateStrings": true,
                    "typeCast": true
                },
                "timezone": '+08:00',
                "logging": false,
            });
        }
        //判断链接是否生效
        if (this.sequelize) {
            this.automatically();
        }
    }

    transaction() {
        return this.sequelize.transaction();
    }


    /**
     * 自动创建机制
     * @return {[type]} [description]
     */
    automatically() {
        //读取对象目录
        let modulesDir = ROOT_DIR + DS + "src" + DS + "modules";
        //遍历循环读取对象列表
        let fileList = this._readDirSync(modulesDir);
        //循环自动创建OMR对象
        fileList.forEach(item => {
            let ModelClass = this._getModelFile(item);
            if (ModelClass && Object.prototype.toString.call(ModelClass) == "[object Function]") {
                let ModelData = new ModelClass();
                let tableFields = ModelData.tableFields();
                this.createTable(ModelData.getTableName(), tableFields);
                //销毁对象
                ModelData = null
            }
        });
    }

    /**
     * 获取API文件
     * @return {[type]} [description]
     */
    _getModelFile = function(model_file) {
        if (is_file(model_file)) {
            delete require.cache[require.resolve(model_file)];
            return require(model_file);
        } else {
            throw new Error('模型文件' + model_file + "不存在,请检查模型目录");
        }
    }

    /**
     * 遍历目录结构数据
     * @param  {[type]} path     [description]
     * @param  {Array}  fileList [description]
     * @return {[type]}          [description]
     */
    _readDirSync(path, fileList = []) {
        let pa = fs.readdirSync(path);
        pa.forEach((ele, index) => {
            let info = fs.statSync(path + DS + ele)
            if (info.isDirectory()) {
                let _list = this._readDirSync(path + DS + ele);
                fileList = fileList.concat(_list)
            } else {
                if (ele != '.DS_Store') {
                    fileList.push(path + DS + ele);
                }

            }
        })
        return fileList;
    }

    /**
     * 获取字段
     * @return {[type]} [description]
     */
    _getField(table, field = null) {
        if (table) {
            if (field) {
                return this._tableFields[table][field] || false
            }
            return this._tableFields[table] || false
        }
        return false
    }

    /**
     * 创建数据库表
     * @return {[type]} [description]
     */
    createTable(tableName, tableFields) {
        //表格前缀处理
        let tableprefix = this.config.tableprefix || "";
        if (tableprefix.charAt(tableprefix.length - 1) == '_') {
            tableprefix = tableprefix.substr(0, tableprefix.length - 1);
        }
        this._tableFields[tableName] = tableFields;
        //用于数据数据库表结构
        tableFields = this.fieldsORM(tableFields);
        const Model = this.sequelize.define(tableName, tableFields, {
            // 时间戳
            timestamps: true,
            // 软删除
            paranoid: true,
            // 前缀
            schema: tableprefix,
            // 连字符
            schemaDelimiter: '_',
            // 字段格式
            createdAt: 'created_time',
            updatedAt: 'updated_time',
            deletedAt: 'deleted_time',
            underscored: false,
            //用于处理时间格式化问题，否则返回的时间为国际时间
            dialectOptions: {
                dateStrings: true,
                typeCast: true
            }
        });
        Model.sync({ alter: true });
    }

    /**
     * 获取数据类型
     * @param  {[type]} type [description]
     * @return {[type]}      [description]
     */
    _getTypeInfo(type) {
        if (type) {
            let typeInfo = type.match(/([\w\W]+)\((.*?)\)/);
            //切割数据类型信息
            if (typeInfo) {
                typeInfo = [typeInfo[1], typeInfo[2]]
            } else {
                typeInfo = [type];
            }
            //匹配返回数据类型
            switch (typeInfo[0].toLowerCase()) {
                case 'int':
                    return typeInfo[1] ? Sequelize.INTEGER(typeInfo[1]) : Sequelize.INTEGER;
                    break;
                case 'bigint':
                    return typeInfo[1] ? Sequelize.BIGINT(typeInfo[1]) : Sequelize.BIGINT;
                    break;
                case 'varchar':
                    return typeInfo[1] ? Sequelize.STRING(typeInfo[1]) : Sequelize.STRING;
                    break;
                case 'text':
                    return Sequelize.TEXT;
                    break;
                case 'json':
                case 'longtext':
                    return Sequelize.TEXT("long");
                    break;
                default:
                    return Sequelize.STRING;
            }
        } else {
            return Sequelize.STRING;
        }
    }

    /**
     * 字段的ORM数据处理
     * @return {[type]} [description]
     */
    fieldsORM(tableFields) {
        let FieldsArray = {};
        for (var key in tableFields) {
            let item = tableFields[key];
            //在原型链上添加方法,用于判断对象上该属性是否存在，存在就返回，否则为空
            Object.defineProperty(item, 'getAttrValue', {
                value(v) {
                    return this.hasOwnProperty(v) ? this[v] : '';
                }
            });
            //初始化字段信息
            let FieldsItem = {};

            //设置参数信息-字段类型
            Object.defineProperty(FieldsItem, 'type', {
                value: this._getTypeInfo(item.type),
                enumerable: true,
                writable: true
            });

            //处理关系形数据库的JSON数据
            if (item.type == "json") {
                FieldsItem['get'] = function(value) {
                    return JSON.parse(this.getDataValue(value));
                }
                FieldsItem['set'] = function(value, key) {
                    this.setDataValue(key, JSON.stringify(value));
                }
            }

            //设置参数信息-默认值
            Object.defineProperty(FieldsItem, 'defaultValue', {
                value: item.getAttrValue('defaultValue') || null,
                enumerable: true,
                writable: true,
            });
            //设置参数信息-是否设置为主键
            Object.defineProperty(FieldsItem, 'primaryKey', {
                value: item.primaryKey ? true : false,
                enumerable: item.hasOwnProperty("primaryKey"),
                writable: true,
            });
            //设置参数信息-设置设置为自动递增
            Object.defineProperty(FieldsItem, 'autoIncrement', {
                value: item.autoIncrement ? true : false,
                enumerable: item.hasOwnProperty("autoIncrement"),
                writable: true,
            });
            //设置参数信息-设置字段注释
            Object.defineProperty(FieldsItem, 'comment', {
                value: item.getAttrValue('label') + item.getAttrValue('comment'),
                enumerable: true,
                writable: true
            });
            //设置参数信息-设置不能为空
            Object.defineProperty(FieldsItem, 'allowNull', {
                value: item.allowNull ? item.allowNull : true,
                enumerable: true,
                writable: true
            });
            FieldsArray[key] = FieldsItem;
        }
        return FieldsArray;
    }

    /**
     * 设置表名
     * @return {[type]} [description]
     */
    name(name) {
        //初始化请空配置
        this.options = {
            name: name.toLowerCase(),
            where: '',
            order: '',
            fields: '',
            paranoid: true
        }
        return this;
    }

    /**
     * [user description]
     * @return {[type]} [description]
     */
    user(id = null) {
        this.setUser(id);
        return this;
    }

    /**
     * [user description]
     * @return {[type]} [description]
     */
    setUser(id = null) {
        let { name } = this.options;
        let fields = this._getField(name);
        if (fields.hasOwnProperty('user_id') && id) {
            this.options['user'] = true;
            this.options.where = { "user_id": id }
        }
        return this;
    }

    /**
     * 设置字段
     * @return {[type]} [description]
     */
    fields() {

    }

    /**
     * 调用软删除的关联数据
     * @return {[type]} [description]
     */
    paranoid(value = false) {
        this.options['paranoid'] = value ? true : false
        return this;
    }


    /**
     * 查询条件
     * @param  {[type]} field [description]
     * @param  {[type]} op    [description]
     * @param  {[type]} value [description]
     * @return {[type]}       [description]
     */
    where(field, op = null, value = null) {
        if (is_string(field) && is_string(op)) {
            let _where = {};
            let _op = this._exp[op];
            _where[field] = {
                [_op]: value
            }
            field = _where;
        }
        //如果第一个元素为数组，且后面两个元素为空的情况下
        if (Object.prototype.toString.call(field) === "[object Array]" && op == null && value == null) {

        }
        //如果第一个元素为数组，第二个元素为OR的时候
        if (Object.prototype.toString.call(field) === "[object Array]" && op == "or" && value == null) {

        }
        this.options['where'] = field;
        //返回实例本身
        return this;
    }

    /**
     * 数据排序
     * @param  {[type]} order [description]
     * @return {[type]}       [description]
     */
    order(order = "", type = 1) {
        this.options['order']
        if (type == 1) {
            this.options['order'] = [order]
        } else {
            this.options['order'] = order
        }
        return this;
    }

    /**
     * 设置数据极限
     * @param  {String} limit [description]
     * @param  {String} end   [description]
     * @return {[type]}       [description]
     * { offset: 5, limit: 5 }
     */
    limit(limit = "", offset = "") {
        //设置参数信息-limit
        Object.defineProperty(this.options, 'limit', {
            value: limit,
            enumerable: limit ? true : false,
            writable: true,
        });
        //设置参数信息-offset
        Object.defineProperty(this.options, 'offset', {
            value: offset,
            enumerable: offset ? true : false,
            writable: true,
        });
        return this;
    }

    /**
     * 用于数据分页
     * @param  {String} page [description]
     * @param  {String} size [description]
     * @return {[type]}      [description]
     * page-number
     * page-size
     * page-total
     * page-count
     */
    page(page = "", size = "") {
        this.options['page'] = true;
        this.options['pageInfo'] = {
            "number": page,
            "size": size,
            "total": 0,
            "count": 0,
        }
        return this;
    }

    /**
     * 执行数据写入
     * @param  {Object} data [description]
     * @return {[type]}      [description]
     */
    async insert(data = {}, options = {}) {
        try {
            let collect = this.sequelize.models[this.options['name']]
            let returned = await collect.create(data, options);
            return returned.toJSON();
        } catch (error) {
            throw error;
        }
    }

    /**
     * 批量插入
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    async bulkCreate(data, options = {}) {
        try {
            let collect = this.sequelize.models[this.options['name']]
            const returned = await collect.bulkCreate(data, options);
            return returned;
        } catch (error) {
            throw error;
        }
    }

    /**
     * 删除数据-默认软删除
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    async delete(options = {}) {
        try {
            let collect = this.sequelize.models[this.options['name']];
            options.where = this.options['where'];
            const returned = await collect.destroy(options);
            return returned;
        } catch (error) {
            throw error;
        }
    }

    /**
     * 真正销毁数据
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    async destroy(options = {}) {
        try {
            let collect = this.sequelize.models[this.options['name']]
            options.where = this.options['where'];
            options.force = true;
            const returned = await collect.destroy(options)
            return returned;
        } catch (error) {
            throw error;
        }
    }

    /**
     * 恢复删除的数据-仅在软删除的下支持
     * @return {[type]} [description]
     */
    async restore(options = {}) {
        try {
            let collect = this.sequelize.models[this.options['name']]
            options.where = this.options['where'];
            const returned = await collect.restore(options)
            return returned;
        } catch (error) {
            throw error;
        }
    }

    /**
     * 更新数据
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    async update(data, options = {}) {
        try {
            let collect = this.sequelize.models[this.options['name']]
            options.where = this.options['where'];
            const returned = await collect.update(data, options);
            return returned;
        } catch (error) {
            throw error;
        }
    }

    /**
     * 选择数据
     * @return {[type]} [description]
     */
    async select() {
        try {
            console.log("我的查询条件信息", this.options)

            let collect = this.sequelize.models[this.options['name']]
            //自定义参数
            let options = {};

            Object.defineProperty(options, 'order', {
                value: this.options['order'],
                enumerable: this.options['order'] ? true : false,
                writable: true,
            });

            Object.defineProperty(options, 'where', {
                value: this.options['where'],
                enumerable: this.options['where'] ? true : false,
                writable: true,
            });

            Object.defineProperty(options, 'limit', {
                value: this.options['limit'],
                enumerable: this.options['limit'] ? true : false,
                writable: true,
            });

            Object.defineProperty(options, 'offset', {
                value: this.options['offset'],
                enumerable: this.options['offset'] ? true : false,
                writable: true,
            });

            Object.defineProperty(options, 'paranoid', {
                value: this.options['paranoid'],
                enumerable: this.hasOwnProperty("paranoid"),
                writable: true,
            });

            //判断是否需要进行数据分页
            if (this.options['page']) {
                //如果该字段存在，需要删除，避免错乱
                if (options['limit']) delete options['limit'];
                if (options['offset']) delete options['offset'];
                let { pageInfo } = this.options;
                //返回总的条数
                const pageObject = await collect.findAndCountAll(options);
                if (pageObject) {
                    pageInfo.total = pageObject.count;
                }
                //获取分多少页
                pageInfo.count = Math.ceil(pageInfo.total / pageInfo.size);
                let where = options.where; //防止数据隔离后条件内数据出错
                //隔离数据设置-此处如果不进行数据格式，无法设置下面的limit
                options = JSON.parse(JSON.stringify(options));
                options.where = where;
                //设置页码和偏移量
                options['limit'] = parseInt(pageInfo.size);
                options['offset'] = parseInt(pageInfo.size) * parseInt(pageInfo.number - 1);
            }
            if (this.options['join']) {
                //获取关联信息数据
                let joinInfo = this.options['join'];

                //获取需要的模型数据
                let JoinModel = this.sequelize.models[joinInfo['name']];

                if (joinInfo.type == "hasOne") {
                    //设置关联字段
                    collect.hasOne(JoinModel, {
                        foreignKey: joinInfo['field'],
                        targetKey: joinInfo['relation'] || "id"
                    })
                    JoinModel.belongsTo(collect, {
                        foreignKey: joinInfo['relation'] || "id"
                    });
                    options['include'] = {
                        model: JoinModel,
                    }
                }

                if (joinInfo.type == "belongsTo") {
                    //设置关联字段
                    JoinModel.hasOne(collect, {
                        foreignKey: joinInfo['relation'] || "id"
                    })
                    collect.belongsTo(JoinModel, {
                        foreignKey: joinInfo['field'],
                        targetKey: joinInfo['relation'] || "id"
                    });
                    options['include'] = {
                        model: JoinModel,
                    }
                }

                if (joinInfo.type == "hasMany") {
                    //设置关联字段
                    collect.hasMany(JoinModel, {
                        foreignKey: joinInfo['field'],
                        targetKey: joinInfo['relation'] || "id"
                    })
                    JoinModel.belongsTo(collect, {
                        foreignKey: joinInfo['relation'] || "id"
                    });
                    options['include'] = {
                        model: JoinModel,
                    }
                }
                //处理where
                if (joinInfo.where) {
                    options['include']['where'] = joinInfo.where;
                }
            }
            //获取数据链接
            const returned = await collect.findAll(options);
            if (returned) {
                if (this.options.hasOwnProperty("pageInfo")) {
                    return {
                        ...this.options.pageInfo,
                        children: returned
                    }
                } else {
                    return returned;
                }
            } else {
                return [];
            }
        } catch (error) {
            throw error;
        }
    }

    /**
     * 单条数据
     * @return {[type]} [description]
     */
    async first() {
        try {
            let collect = this.sequelize.models[this.options['name']];
            let options = {};

            Object.defineProperty(options, 'where', {
                value: this.options['where'],
                enumerable: this.options['where'] ? true : false,
                writable: true,
            });

            Object.defineProperty(options, 'paranoid', {
                value: this.options['paranoid'],
                enumerable: this.hasOwnProperty("paranoid"),
                writable: true,
            });

            //处理关联查询用的
            if (this.options['join']) {
                //获取关联信息数据
                let joinInfo = this.options['join'];

                //获取需要的模型数据
                let JoinModel = this.sequelize.models[joinInfo['name']];

                if (joinInfo.type == "hasOne") {
                    //设置关联字段
                    collect.hasOne(JoinModel, {
                        foreignKey: joinInfo['field'],
                        as: joinInfo.as
                    })
                    JoinModel.belongsTo(collect, { foreignKey: "id" });
                    options['include'] = joinInfo.as;
                }

                if (joinInfo.type == "belongsTo") {
                    //设置关联字段
                    JoinModel.hasOne(collect, {
                        foreignKey: "id"
                    })
                    collect.belongsTo(JoinModel, { foreignKey: joinInfo['field'], as: joinInfo.as });
                    options['include'] = joinInfo.as;
                }

                if (joinInfo.type == "hasMany") {
                    //设置关联字段
                    collect.hasMany(JoinModel, {
                        foreignKey: joinInfo['field'],
                        as: joinInfo.as
                    })
                    JoinModel.belongsTo(collect, { foreignKey: "id" });
                    options['include'] = joinInfo.as;
                }
            }
            const returned = await collect.findOne(options);
            if (returned) {
                return returned.toJSON();
            } else {
                return '';
            }
        } catch (error) {
            throw error;
        }
    }

    /**
     * 处理试图关联查询
     * @param  {[type]}  join  [description]
     * @param  {Boolean} field [description]
     * @param  {[type]}  on    [description]
     * @param  {String}  type  [description]
     * @return {[type]}        [description]
     */
    view(join, field = '', on = null, type = 'INNER') {
        // this.options['view'] = true;
        // this.options['join'] = {
        //     name: join
        //     id: field
        // }
        return this;
    }

    /**
     * 获取Join表名及别名 支持
     * @param  {[type]} join  [description]
     * @param  {[type]} alias [description]
     * @return {[type]}       [description]
     */
    getJoinTable(join, alias = null) {

    }

    /**
     * 查询SQL组装 join
     * @access public
     * @param mixed  $join      关联的表名
     * @param mixed  $condition 条件
     * @param string $type      JOIN类型
     * @return $this
     */
    join($join, $condition = null, $type = 'INNER') {

    }
}

class sql {
    /**
     * 数据库连接实例
     * @type {Array}
     */
    static instance = [];

    /**
     * 数据库初始化，并取得数据库类实例
     * @access public
     * @param  mixed       config 连接配置
     * @param  bool|string name   连接标识 true 强制重新连接
     * @return Connection
     * @throws Exception
     */
    static connect(config = [], name = false) {
        if (false === name) {
            name = md5(to_json(config));
        }
        if (true === name || !isset(this.instance[name])) {
            let options = config;
            this.instance[name] = new database(options);
        }
        return this.instance[name];
    }
}

module.exports = sql;