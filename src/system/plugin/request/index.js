/*
 * @Author: Sean
 * @Date:   2019-09-19 03:25:22
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-17 17:31:19
 */
const http = require('axios');

class request {
    /**
     * 构造函数
     * @return {[type]} [description]
     */
    constructor() {

    }

    /**
     * 以get方式提交请求
     * @param $url
     * @return bool|mixed
     */
    async get($url) {
        return await http({
            url: $url,
            method: 'GET',
        }).then(res => {
            if (res.status === 200) {
                return res.data;
            } else {
                return false;
            }
        })
    }

    /**
     * 以post方式提交请求
     * @param string $url
     * @param array|string $data
     * @return bool|mixed
     */
    async post($url, $data) {
        return await http({
            url: $url,
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            data: $data
        }).then(res => {
            if (res.status === 200) {
                return res.data;
            } else {
                return false;
            }
        })
    }

    /**
     * 发送请求
     * @param  [type] $options [description]
     * @return [type]          [description]
     */
    async send($options) {
        return await http($options).then(res => {
            if (res.status === 200) {
                return res.data;
            } else {
                return false;
            }
        })
    }
}
module.exports = request;