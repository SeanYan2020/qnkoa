/*
 * @Author: qinuoyun
 * @Date:   2021-09-25 14:14:39
 * @Last Modified by:   wiki
 * @Last Modified time: 2021-10-16 00:20:46
 */
const Common = require('./lib/Common');
const WEAPP_PREFIX = 'https://api.weixin.qq.com';
const WEAPP_AUTH_URL = '/sns/jscode2session?';

class WechatWeapp extends Common {

	/**
	 * 小程序登录
	 * @param  {String} $code      [description]
	 * @param  {String} $appid     [description]
	 * @param  {String} $appsecret [description]
	 * @return {[type]}            [description]
	 */
	async weappLogin( $code = '',$appid = '', $appsecret = ''){

		if (!$appid || !$appsecret) {
            $appid = this.$appid;
            $appsecret = this.$appsecret
        }

        let url = WEAPP_PREFIX + WEAPP_AUTH_URL + 'appid=' + $appid + '&secret=' + $appsecret+'&js_code='+$code+'&grant_type=authorization_code';
		
        let result = await request.get(url);

        if (result) {
            if (result.errcode) {
                this.errCode = result['errcode'];
                this.errMsg = result['errmsg'];
                return false;
            }

            return result;
        }
        return false;

	}
}
module.exports = WechatWeapp;