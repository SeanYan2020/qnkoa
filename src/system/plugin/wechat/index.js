/*
 * @Author: qinuoyun
 * @Date:   2021-09-25 13:43:40
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-25 14:39:30
 */
const path = require('path');
//存储缓存数据
global.__wechat_cache__ = {};

class wehcat {
    /**
     * 事件注册函数
     * @var array
     */
    static $callback = {};

    /**
     * 配置参数
     * @var array
     */
    static $config = {};

    /**
     * 对象缓存
     * @var array
     */
    static $cache = {};

    /**
     * 获取微信SDK接口对象(别名函数)
     * @param string $type 接口类型(Card|Custom|Device|Extends|Media|Menu|Oauth|Pay|Receive|Script|User|Poi)
     * @param array $config SDK配置(token,appid,appsecret,encodingaeskey,mch_id,partnerkey,ssl_cer,ssl_key,qrc_img)
     * @return WechatCard|WechatCustom|WechatDevice|WechatExtends|WechatMedia|WechatMenu|WechatOauth|WechatPay|WechatPoi|WechatReceive|WechatScript|WechatService|WechatUser
     */
    static get_instance($type, $config = {}) {
        return this.get($type, $config);
    }

    /**
     * 获取微信SDK接口对象
     * @param string $type 接口类型(Card|Custom|Device|Extends|Media|Menu|Oauth|Pay|Receive|Script|User|Poi)
     * @param array $config SDK配置(token,appid,appsecret,encodingaeskey,mch_id,partnerkey,ssl_cer,ssl_key,qrc_img)
     * @return WechatCard|WechatCustom|WechatDevice|WechatExtends|WechatMedia|WechatMenu|WechatOauth|WechatPay|WechatPoi|WechatReceive|WechatScript|WechatService|WechatUser
     */
    static get($type, $config = {}) {
        let _object = {};
        let _config = Object.assign(this.$config, $config);
        let $index = md5($type.toUpperCase() + md5(JSON.stringify($config)));

        console.log('$index', $index)

        if (!__wechat_cache__[$index]) {
            console.log("我是新的创建")
            //获取对应的API数据文件
            const instance = getSdkFile($type);
            //判断是否函数
            if (typeof instance == 'function') {
                __wechat_cache__[$index] = new instance();
            } else {
                throw new Error('API文件' + api_file + "非类文件或无module.exports输出");
            }
        }
        //直接返回对象方法
        return __wechat_cache__[$index];
    }

    /**
     * 设置配置参数
     * @param array $config
     * @return array
     */
    static config($config = {}) {
        if (Object.keys($config).length) {
            this.$config = Object.assign(this.$config, $config);
        }

        if (!this.$config['component_verify_ticket']) {
            this.$config['component_verify_ticket'] = __wechat_cache__['component_verify_ticket'];
        }
        if (!this.$config['token'] && this.$config['component_token']) {
            this.$config['token'] = this.$config['component_token'];
        }
        if (!this.$config['appsecret'] && this.$config['component_appsecret']) {
            this.$config['appsecret'] = this.$config['component_appsecret'];
        }
        if (!this.$config['encodingaeskey'] && this.$config['component_encodingaeskey']) {
            this.$config['encodingaeskey'] = this.$config['component_encodingaeskey'];
        }
        return this.$config;
    }

}


/**
 * 获取API文件
 * @return {[type]} [description]
 */
const getSdkFile = function(Model) {
    const sdk_file = path.resolve(__dirname) + DS + "Wechat" + ucfirst(Model) + '.js';
    console.log("sdk_file", sdk_file)
    if (is_file(sdk_file)) {
        delete require.cache[require.resolve(sdk_file)];
        return require(sdk_file);
    } else {
        throw new Error('sdk文件' + sdk_file + "不存在,请检查");
    }
}

module.exports = wehcat;