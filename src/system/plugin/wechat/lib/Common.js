/*
 * @Author: qinuoyun
 * @Date:   2021-09-25 14:14:39
 * @Last Modified by:   wiki
 * @Last Modified time: 2021-10-15 22:23:39
 */

/** API接口URL需要使用此前缀 */
const API_BASE_URL_PREFIX = 'https://api.weixin.qq.com';
const API_URL_PREFIX = 'https://api.weixin.qq.com/cgi-bin';
const GET_TICKET_URL = '/ticket/getticket?';
const AUTH_URL = '/token?grant_type=client_credential&';

const Loader = require('../index.js');

class Common {
    $token = '';
    $encodingAesKey = '';
    $encrypt_type = '';
    $appid = '';
    $appsecret = '';
    $access_token = '';
    $postxml = '';
    $_msg = '';
    $errCode = 0;
    $errMsg = '';
    $config = {};
    $_retry = false;

    /**
     * 构造方法
     * @param array $options
     */
    constructor($options) {
        let $config = Loader.config($options);
        this.$token = $config['token'] || '';
        this.$appid = $config['appid'] || '';
        this.$appsecret = $config['appsecret'] || '';
        this.$encodingAesKey = $config['encodingaeskey'] || '';
        this.$config = $config;
    }

    /**
     * 获取当前操作公众号APPID
     * @return string
     */
    getAppid() {
        return this.$appid;
    }

    /**
     * 获取SDK配置参数
     * @return array
     */
    getConfig() {
        return this.$config;
    }

    /**
     * 当前当前错误代码
     * @return int
     */
    getErrorCode() {
        return this.$errCode;
    }

    /**
     * 获取当前错误内容
     * @return string
     */
    getError() {
        return this.$errMsg;
    }

    /**
     * 获取公众号访问 access_token
     * @param string $appid 如在类初始化时已提供，则可为空
     * @param string $appsecret 如在类初始化时已提供，则可为空
     * @param string $token 手动指定access_token，非必要情况不建议用
     * @return bool|string
     */
    async getAccessToken($appid = '', $appsecret = '', $token = '') {
        if (!$appid || !$appsecret) {
            $appid = this.$appid;
            $appsecret = this.$appsecret
        }
        if ($token) {
            return this.$access_token = $token;
        }

        //判断缓存中是否存在数据
        let cache_key = 'wechat_access_token_'.$appid;
        let access_token = "";
        if ((access_token = Tools.getCache(cache_key)) && access_token) {
            return this.$access_token = access_token;
        }

        //请求返回数据
        let $result = await Tools.httpGet(API_URL_PREFIX + AUTH_URL + 'appid=' + $appid + '&secret=' + $appsecret);

        if ($result) {
            if (!$result || $result.errcode) {
                this.$errCode = $result['errcode'];
                this.$errMsg = $result['errmsg'];
                console.log("Get New AccessToken Error. {$this->errMsg}[{$this->errCode}]", "ERR - {$this->appid}");
                return false;
            }
            this.$access_token = $result['access_token'];
            console.log("Get New AccessToken Success.", "MSG - {this.appid}");
            Tools.setCache(cache_key, this.$access_token, 5000);
            return this.$access_token;
        }
        return false;
    }


}

module.exports = Common;