/*
 * @Author: qinuoyun
 * @Date:   2021-09-16 09:38:42
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-24 14:15:06
 */
module.exports = {
    "database": "mongodb",
    "whitelist": [
        /^\/api\/login/,
        // "/api/user/index",
        // "/api/account/index"
        // "/api/question/update"
    ],
    "mongodb": {
        "hostname": "weixin.gongqi.info",
        "database": "gqimos",
        "username": "",
        "password": "",
        "replicaSet": "",
        "hostport": "27017",
        "tableprefix": "qn_"
    }
}