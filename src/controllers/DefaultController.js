/*
 * @Author: seanyan
 * @Date:   2021-09-13 16:21:33
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-24 15:04:00
 */
class DefaultController {
    async actionIndex() {
        return await this.setArgument();
    }
    
    actionUsers() {
        return new Promise((resolve, reject) => {
            resolve(111111)
        })
    }

    setArgument() {
        return 112222;
    }
}

module.exports = DefaultController;