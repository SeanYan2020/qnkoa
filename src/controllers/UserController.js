/*
 * @Author: seanyan
 * @Date:   2021-09-13 16:21:33
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-18 22:26:58
 */
class UserController {
    actionIndex() {
        return new Promise((resolve, reject) => {
            resolve("actionIndex")
        })
    }

    actionDemo() {
        return new Promise((resolve, reject) => {
            resolve("actionDemo")
        })
    }

    /**
     * 用户信息
     * @return {[type]} [description]
     */
    actionUserinfo() {
        return new Promise((resolve, reject) => {
            resolve("actionUserinfo")
        })
    }

    /**
     * 获取消息列表
     * @return {[type]} [description]
     */
    actionMessage() {
        return new Promise((resolve, reject) => {
            resolve("actionMessage")
        })
    }
}
module.exports = UserController;