/*
 * @Author: seanyan
 * @Date:   2021-09-13 16:21:33
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-18 17:29:42
 */
class SocketController {
    /**
     * Socket用户登陆
     * @return {[type]} [description]
     */
    async actionLogin(socket, data) {
        //执行加入房间，以用户ID为房间号
        await socket.join(data.userId);
        //需要批量处理加入群聊
        //1.数据库进行数据查询
        //2.获取数据后进行循环操作
    }

    /**
     * 发送消息接口
     * @return {[type]} [description]
     */
    actionSend(socket, message) {
        let tableNmae = this.getTableNmae();
        //修改消息状态
        message.status = "succeed";
        //将数据存入数据库
        db.name(tableNmae).insert(message).then(async data => {
            //将数据发送给对应的用户
            await socket.to(message.toContactId).emit("Receive", message);
            //通知客户端消息接收成功
            await socket.emit("IsReceived", { status: "succeed", id: message.id });
            console.log("消息发送成功", message)
        }).catch(error => {
            //通知客户端消息接收失败
            socket.emit("IsReceived", { status: "failed", id: data.id });
            console.log("消息发送失败", error)
        })
    }

    /**
     * 加入分组
     * @return {[type]} [description]
     */
    actionJoinGroup() {
        return new Promise((resolve, reject) => {

        })
    }

    /**
     * 处理消息数据以否已读
     * @return {[type]} [description]
     */
    actionIsRead(socket, data) {
        console.log("接收的已读消息数据", data)
        const { id, toContactId, sendTime } = data;

        // const $record = getModel();
        // await $record.updateMany({ recordId: id }, { isRead: 1 });

        //处理表格数据
        let tableNmae = this.getTableNmae(sendTime);

        console.log("查看表格", tableNmae)

        db.name(tableNmae).where({
            "id": id,
            "toContactId": toContactId
        }).update({ "isRead": 1 }).then(data => {
            console.log("修改消息数据状态", data)
        }).catch(error => {

        })

        // if(typeof id == "object") await socket.emit("sendIsReadReady", obj);
        // await socket.to(toContactId).emit("isRead", obj);
    }

    /**
     * 获取数据库时间表
     * @param  {[type]} value [description]
     * @return {[type]}       [description]
     */
    getTableNmae(value) {
        let date = new Date();
        let year, month
        if (value) {
            date = new Date(value);
        }
        year = date.getFullYear();
        const rawMonth = (date.getMonth() + 1).toString();
        month = rawMonth.length == 1 ? "0" + rawMonth : rawMonth;
        return `${year}-${month}`;
    }

    /**
     * 获取数据库时间表
     * @param  {[type]} value [description]
     * @return {[type]}       [description]
     */
    getTableNmae2(value) {
        const date = new Date();
        let year, month
        if (!value) {
            year = date.getFullYear();
            const rawMonth = (date.getMonth() + 1).toString();
            month = rawMonth.length == 1 ? "0" + rawMonth : rawMonth;
        } else {
            let rawMonth = date.getMonth()
            year = date.getFullYear()

            rawMonth = rawMonth - value
            if (rawMonth < 0) {
                rawMonth = 12 + rawMonth
                year = year - 1
            }
            rawMonth = (rawMonth + 1).toString();
            month = rawMonth.length == 1 ? "0" + rawMonth : rawMonth;
        }
        return `${year}-${month}`;
    }
}

module.exports = SocketController;