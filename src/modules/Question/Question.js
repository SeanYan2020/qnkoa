/*
 * @Author: qinuoyun
 * @Date:   2021-09-19 21:10:34
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-24 13:52:32
 */
class Question extends CommonModels {
    /**
     * 用于字段处理
     * @return {[type]} [description]
     */
    tableFields() {
        return {
            //题目名称
            "question_title": "",
            //题目答案
            "question_answer": "",
            //题目选项
            "question_options": {
                "id": "B",
                "content": "错",
                "type": 1,
            },
            //题目类型
            "question_type": "",
            //隶属ID
            "paper_uniqueid": ""
        }
    }


    /**
     * @inheritdoc
     */
    tableName() {
        return '{{%question}}';
    }

    /**
     * 实现数据验证
     * 需要数据写入，必须在rules添加对应规则
     * 在控制中执行[模型]->attributes = $postData;
     * 否则会导致验证不生效，并且写入数据为空
     * @return [type] [description]
     */
    rules() {
        return {
            question_title: [
                { type: "string", required: true },
                { min: 3, max: 5, message: '长度在 3 到 5 个字符' }
            ],
            question_answer: [
                { type: "string", required: true }
            ]
        }
    }



}

module.exports = Question;