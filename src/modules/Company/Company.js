/*
 * @Author: qinuoyun
 * @Date:   2021-09-19 21:10:34
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-24 22:01:53
 */
class Company extends CommonModels {
    /**
     * 用于字段处理
     * @return {[type]} [description]
     */
    tableFields() {
        return {
            //名称
            "company_name": "",
            //人数
            "company_people": "",
            //标签
            "company_tag": "",
            //地址
            "company_address": "",
            //介绍
            "company_about": "",
            //教学视频
            "company_video": "",
            //相册
            "company_album": "",
            //选择题库
            "company_paper": ""
        }
    }

    /**
     * 实现数据验证
     * 需要数据写入，必须在rules添加对应规则
     * 在控制中执行[模型]->attributes = $postData;
     * 否则会导致验证不生效，并且写入数据为空
     * @return [type] [description]
     */
    rules() {
        return {
            company_name: [
                { type: "string", required: true },
                { min: 3, max: 5, message: '长度在 3 到 5 个字符' }
            ],
            company_people: [
                { type: "string", required: true }
            ]
        }
    }

    /**
     * @inheritdoc
     */
    tableName() {
        return '{{%company}}';
    }

}

module.exports = Company;