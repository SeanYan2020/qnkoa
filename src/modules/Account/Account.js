/*
 * @Author: qinuoyun
 * @Date:   2021-09-19 21:10:34
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-24 22:02:04
 */
class Account extends CommonModels {
    /**
     * 用于字段处理
     * @return {[type]} [description]
     */
    tableFields() {
        return {
            //手机号
            "mobile": "",
            //密码
            "password": ""
        }
    }

    /**
     * 实现数据验证
     * 需要数据写入，必须在rules添加对应规则
     * 在控制中执行[模型]->attributes = $postData;
     * 否则会导致验证不生效，并且写入数据为空
     * @return [type] [description]
     */
    rules() {
        return {
            mobile: [
                { type: "string", required: true }
            ],
            password: [{
                type: "string",
                required: true
            }]
        }
    }

    /**
     * @inheritdoc
     */
    tableName() {
        return '{{%account}}';
    }
}

module.exports = Account;