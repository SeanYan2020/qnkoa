/*
 * @Author: qinuoyun
 * @Date:   2021-09-19 21:10:34
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-24 22:01:46
 */
class Paper extends CommonModels {
    /**
     * 用于字段处理
     * @return {[type]} [description]
     */
    tableFields() {
        return {
            //名称
            "paper_name": "",
            //标识
            "paper_uniqueid": "",
            //按钮
            "paper_button": "",
            //抽取数量
            "paper_extract": 10
        }
    }

    /**
     * 实现数据验证
     * 需要数据写入，必须在rules添加对应规则
     * 在控制中执行[模型]->attributes = $postData;
     * 否则会导致验证不生效，并且写入数据为空
     * @return [type] [description]
     */
    rules() {
        return {
            paper_name: [
                { type: "string", required: true },
                { min: 3, max: 5, message: '长度在 3 到 5 个字符' }
            ],
            paper_button: [
                { type: "string", required: true }
            ]
        }
    }

    /**
     * @inheritdoc
     */
    tableName() {
        return '{{%paper}}';
    }
}

module.exports = Paper;