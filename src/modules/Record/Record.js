/*
 * @Author: qinuoyun
 * @Date:   2021-09-19 21:10:34
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-24 22:19:51
 */
class Record extends CommonModels {
    /**
     * 用于字段处理
     * @return {[type]} [description]
     */
    tableFields() {
        return {
            //标识
            "userID": "",
            //记录时间
            "record_time": "",
            //答题分数
            "record_number": 0,
            //公司ID
            "company_id": ""
        }
    }

    /**
     * 实现数据验证
     * 需要数据写入，必须在rules添加对应规则
     * 在控制中执行[模型]->attributes = $postData;
     * 否则会导致验证不生效，并且写入数据为空
     * @return [type] [description]
     */
    rules() {
        return {
            record_time: [
                { type: "string", required: true },
            ],
            userID: [
                { type: "string", required: true }
            ]
        }
    }

    /**
     * @inheritdoc
     */
    tableName() {
        return '{{%record}}';
    }
}

module.exports = Record;