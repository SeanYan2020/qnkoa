/*
 * @Author: qinuoyun
 * @Date:   2021-09-15 22:43:53
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-17 14:16:06
 */
//初始化常量
require('./system/initialize.js')
require('./system/functions.js')
require('./system/config/common.js')

//初始化数据库插件
if (C("database") && C("database") == 'mongodb') {
    const _mongodb = require('./plugin/mongodb')()
    global.db = new _mongodb(C("mongodb"));
}


//加载系统需要数据
const app = require('koa')(),
    logger = require('koa-logger'),
    json = require('koa-json'),
    cors = require('koa-cors'),
    views = require('koa-views'),
    socket = require('koa-socket'),
    onerror = require('koa-onerror'),
    koaJwt = require('koa-jwt');

//错误信息收纳器
onerror(app);

//设置KOA视图加载目录
app.use(views('views', {
    root: __dirname + '/views',
    default: 'ejs'
}));
//处理跨域问题
app.use(cors());
//获取数据内容
app.use(require('koa-bodyparser')());
//加载JSON中间件
app.use(json());
//加载日志中间件
app.use(logger());

//处理运行时间
app.use(function*(next) {
    var start = new Date;
    yield next;
    var ms = new Date - start;
    console.log('%s %s - %s', this.method, this.url, ms);
});

// 设置静态目录
app.use(require('koa-static')(__dirname + '/src/public'));


// 加载路由信息
var index = require('./system/views');
app.use(index.routes(), index.allowedMethods());


// 加载API路由
var API = require('./system/plugin/api');
app.use(API.routes(), API.allowedMethods());


// //JWT中间件
// app.use(koaJwt({
//     secret: C("components.jwt.key")
// }).unless({
//     path: ['/api/login/index', /^\/socket\.io/] //除了这个地址，其他的URL都需要验证
// }));
// // 加载JWT验证信息
// app.use(function*(next) {
//     let token = this.headers.authorization;
//     if (token == undefined) {
//         yield next;
//     } else {
//         console.log("被拦截了2")
//         yield next;
//     }

//     // if (token == undefined) {
//     //     await next();
//     // } else {
//     //     verToken.verToken(token).then((data) => {
//     //         //这一步是为了把解析出来的用户信息存入全局state中，这样在其他任一中间价都可以获取到state中的值
//     //         ctx.state = {
//     //             data: data
//     //         };
//     //     })
//     //     await next();
//     // }
// })

//错误状态码处理 SocketController
// app.use(async (ctx, next) => {
//     return next().catch((err) => {
//         if (401 == err.status) {
//             ctx.status = 401;
//             ctx.body = {
//                 status: 401,
//                 msg: '登录过期，请重新登录'
//             }
//         } else {
//             throw err;
//         }
//     });
// });



// console.log("component", Object.keys(new component()))

//判断是否需要开启IO链接
if (C("components.socket.enableConnection")) {
    try {
        let caomponent = getComponent(C("components.socket.class"));
        //获取所有链接的方法名
        let connections = Object.getOwnPropertyNames(caomponent.prototype);

        // console.log("connections", connections)
        // for (let index in connections) {
        //     let onAction = connections[index].match(/action([\w]+)/);
        //     if (onAction && onAction[1]) {
        //         console.log("action", onAction[1])
        //     }
        // }

        console.log("执行到此处了")

        app._io.on('connection', socket => {
            console.log('建立连接了');
            //实力话组建对象
            let socketClass = new caomponent(socket);
            //循环创建会话
            for (let index in connections) {
                let action = connections[index];
                let onAction = action.match(/action([\w]+)/);
                if (onAction && onAction[1]) {
                    console.log("action", onAction[1])
                    //接收数据
                    socket.on("login", async function(data) {
                        socketClass[action](socket, data)
                    });
                }

            }
        })

    } catch (error) {
        throw error;
    }
}

// 错误信息拦截
app.on('error', (err, ctx) => {
    console.error('server error', err)
});

module.exports = app;