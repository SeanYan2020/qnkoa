/*
 * @Author: qinuoyun
 * @Date:   2021-09-22 11:03:23
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2021-09-22 11:04:05
 */
const crypto = require('crypto');

module.exports = {
    MD5_SUFFIX: 'W4xOLXEvo*$8pk%aruoV!&t$5NAp0HHl',
    md5: function(str) {
        var obj = crypto.createHash('md5');
        obj.update(str);
        return obj.digest('hex');
    }
}